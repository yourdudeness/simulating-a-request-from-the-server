window.addEventListener('DOMContentLoaded', () => {

    let Response = [{
            src: 'img/tabs/vegy.jpg',
            imgTitle: 'vegy',
            title: 'Меню "Фитнес"',
            description: 'Меню "Фитнес" - это новый подход к приготовлению блюд: больше свежих овощей и фруктов. Продукт активных и здоровых людей. Это абсолютно новый продукт с оптимальной ценой и высоким качеством!',
            price: 9,
            parentBlock: ".menu .container",
            classDiv: 'menu__item'
        },
        {
            src: 'img/tabs/post.jpg',
            imgTitle: 'vegy',
            title: 'Меню "Фитнес"',
            description: 'Меню "Фитнес" - это новый подход к приготовлению блюд: больше свежих овощей и фруктов. Продукт активных и здоровых людей. Это абсолютно новый продукт с оптимальной ценой и высоким качеством!',
            price: 9,
            parentBlock: ".menu .container",
            classDiv: 'menu__item'
        },
        {
            src: 'img/tabs/elite.jpg',
            imgTitle: 'vegy',
            title: 'Меню "Фитнес"',
            description: 'Меню "Фитнес" - это новый подход к приготовлению блюд: больше свежих овощей и фруктов. Продукт активных и здоровых людей. Это абсолютно новый продукт с оптимальной ценой и высоким качеством!',
            price: 9,
            parentBlock: ".menu .container",
            classDiv: 'menu__item'
        },
    ]

    const xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://mysite.ru/api/getitems');
    xhr.send();
    xhr.onload = function () {
        Response = xhr.response;
    };
    xhr.onerror = function () {
        console.log();
        ("Запрос не удался");
    };

    class MenuCard {
        constructor(src, alt, title, descr, price, parentSelector, ...classes) {
            this.src = src;
            this.alt = alt;
            this.title = title;
            this.descr = descr;
            this.price = price;
            this.classes = classes;
            this.transfer = 27;
            this.parent = document.querySelector(parentSelector);

        }

        changeToUA() {
            this.price = this.price * this.transfer;
        }

        render() {
            const elem = document.createElement('div');
            if (this.classes.length === 0) {
                this.elem = 'menu__item';
                elem.classList.add(this.elem)
            } else {
                this.classes.forEach(className => elem.classList.add(className));
            }

            elem.innerHTML = `

            <img src=${this.src} alt=${this.alt}>
            <h3 class="menu__item-subtitle">${this.title}</h3>
            <div class="menu__item-descr">${this.descr}</div>
            <div class="menu__item-divider"></div>
            <div class="menu__item-price">
                <div class="menu__item-cost">Цена:</div>
                <div class="menu__item-total"><span>${this.price}</span> грн/день</div>
            </div>

            `;

            this.parent.append(elem);
        }
    }


    Response.forEach(item => {
        new MenuCard(
            item.src,
            item.imgTitle,
            item.title,
            item.description,
            item.price,
            item.parentBlock,
            item.classDiv
        ).render();
    })

    //tabs

    const tabsContent = document.querySelectorAll('.tabcontent'),
        tabsItem = document.querySelectorAll('.tabheader__item'),
        tabsParent = document.querySelector('.tabheader__items');

    function hideTabs() {
        tabsContent.forEach((item) => {
            item.classList.remove('show');
            item.classList.add('hide');
        });

        tabsItem.forEach((item) => {
            item.classList.remove('tabheader__item_active');
        })
    }

    function showTabs(i = 0) {
        tabsContent[i].classList.remove('hide');
        tabsContent[i].classList.add('show');
        tabsItem[i].classList.add('tabheader__item_active');
    }

    hideTabs();
    showTabs();

    tabsParent.addEventListener('click', (event) => {
        const target = event.target;

        if (target && target.classList.contains('tabheader__item')) {
            tabsItem.forEach((event, item) => {
                if (target == event) {
                    hideTabs();
                    showTabs(item);
                }
            })
        }
    })

})